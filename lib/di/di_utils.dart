import 'package:challenge_app/block/profile/profile_bloc.dart';
import 'package:challenge_app/block/searches/searches_bloc.dart';
import 'package:challenge_app/database/data_sources/challenges_ds.dart';
import 'package:challenge_app/database/data_sources/users_ds.dart';
import 'package:challenge_app/database/user_service.dart';
import 'package:challenge_app/view/page/mape_page_controller.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

import '../block/map/map_bloc.dart';
import '../block/messages/messages_bloc.dart';
import '../database/data_sources/requests_ds.dart';
import '../database/data_sources/teams_ds.dart';
import '../database/fb_api.dart';
import '../database/fb_service.dart';
import '../database/data_sources/messages_ds.dart';
import '../database/place_api_service.dart';
import '../database/positioning_service.dart';
import '../domain/interactor/interactor.dart';
import '../domain/model/search_filter.dart';
import '../domain/model/user_settings.dart';

final injector = GetIt.instance;

Future<void> initDependencies() async {
  injector.registerSingleton<UserService>(UserService());

  var dataDirectory = await getApplicationDocumentsDirectory();
  Hive.init(dataDirectory.path);
  Hive.registerAdapter(UserSettingsAdapter());
  Hive.registerAdapter(SearchFilterAdapter());
  Hive.registerAdapter(SearchFilterEnumAdapter());

  injector.registerSingleton<FBApi>(FBService());

  injector.registerSingleton(TeamsDataSource(injector<FBApi>()));
  injector.registerSingleton(ChallengesDataSource(injector<FBApi>()));
  injector.registerSingleton(UsersDataSource(injector<FBApi>()));
  injector.registerSingleton(MessagesDataSource(injector<FBApi>()));
  injector.registerSingleton(RequestsDataSource(injector<FBApi>()));

  injector.registerSingleton<MapPageController>(MapPageController());
  injector.registerSingleton<PositioningService>(PositioningService());
  injector.registerSingleton<PlaceApiService>(PlaceApiService());

  Box<UserSettings> settingBox = Hive.isBoxOpen('userSettings')
      ? Hive.box('userSettings')
      : await Hive.openBox('userSettings');
  Box<SearchFilter> filterBox = Hive.isBoxOpen('searchFilter')
      ? Hive.box('searchFilter')
      : await Hive.openBox('searchFilter');

  UserSettings? userSettings;
  if (settingBox.isEmpty) {
    userSettings = UserSettings();
    settingBox.add(userSettings);
  } else {
    userSettings = settingBox.getAt(0);
  }
  injector.registerSingleton<UserSettings>(userSettings!);

  SearchFilter? searchFilter;
  if (filterBox.isEmpty) {
    searchFilter = SearchFilter();
    filterBox.add(searchFilter);
  } else {
    searchFilter = filterBox.getAt(0);
  }
  injector.registerSingleton<SearchFilter>(searchFilter!);

  injector.registerSingletonAsync(
    () async {
      return Interactor(
        injector<TeamsDataSource>(),
        injector<ChallengesDataSource>(),
        injector<UsersDataSource>(),
        injector<MessagesDataSource>(),
        injector<RequestsDataSource>(),
        filterBox,
        settingBox,
        userSettings!,
      );
    },
  );

  injector.registerFactory(
    () => SearchesBloc(
      injector<Interactor>(),
    ),
  );

  injector.registerFactory(
        () => MapBloc(
      injector<Interactor>(),
    ),
  );

  injector.registerFactory(
    () => MessagesBloc(
      injector<Interactor>(),
    ),
  );

  injector.registerFactory(
    () => ProfileBloc(
      injector<Interactor>(),
    ),
  );
}
