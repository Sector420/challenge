import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../domain/interactor/interactor.dart';
import '../../domain/model/message.dart';

part 'messages_event.dart';

part 'messages_state.dart';

class MessagesBloc extends Bloc<MessagesEvent, MessagesState> {
  final Interactor _interactor;

  MessagesBloc(this._interactor) : super(MessagesInitial()) {
    on<LoadMessages>((event, emit) async {
      await emit.forEach(
        _interactor.getMessages(event.teamId),
        onData: (messages) => MessagesLoaded(messages),
      );
    });
    on<UploadMessage>((event, emit) {
      _interactor.addMessage(event.teamId, event.newMessage);
      emit(PictureDisable());
    });
    on<FilePickedEvent>((event, emit) async {
      final result = await FilePicker.platform.pickFiles();
      if (result != null) {
        emit(FileSelected(result));
      }
    });
    on<DeletePicture>((event, emit) async {
      emit(PictureDisable());
    });
  }
}
