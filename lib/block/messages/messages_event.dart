part of 'messages_bloc.dart';

@immutable
abstract class MessagesEvent {}

class LoadMessages extends MessagesEvent {
  final String teamId;

  LoadMessages(this.teamId);
}

class UploadMessage extends MessagesEvent {
  final String teamId;
  final Message newMessage;

  UploadMessage(this.teamId, this.newMessage);
}

class FilePickedEvent extends MessagesEvent {

  FilePickedEvent();
}

class DeletePicture extends MessagesEvent {

  DeletePicture();
}