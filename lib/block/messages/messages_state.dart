part of 'messages_bloc.dart';

@immutable
abstract class MessagesState {}

class MessagesInitial extends MessagesState {}

class MessagesLoaded extends MessagesState {
  final List<Message> messages;

  MessagesLoaded(this.messages);
}

class FileSelected extends MessagesState {
  final FilePickerResult? result;

  FileSelected(this.result);
}

class PictureDisable extends MessagesState {

  PictureDisable();
}