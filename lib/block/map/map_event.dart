part of 'map_bloc.dart';

@immutable
abstract class MapEvent {}

class LoadChallenges extends MapEvent {
  LoadChallenges();
}

class UploadChallenge extends MapEvent {
  final Challenge newChallenge;

  UploadChallenge(this.newChallenge);
}

class UploadRequest extends MapEvent {
  final Request newRequest;

  UploadRequest(this.newRequest);
}

class Refresh extends MapEvent {
  Refresh();
}

class RegisterCurrentPosition extends MapEvent {
  RegisterCurrentPosition();
}

class SearchPlaces extends MapEvent {
  final String input;
  final String lang;

  SearchPlaces(this.input, this.lang);
}
