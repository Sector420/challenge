part of 'map_bloc.dart';

@immutable
abstract class MapState {}

class MapInitial extends MapState {}

class ChallengesLoaded extends MapState {
  final List<Challenge> challenges;

  ChallengesLoaded(this.challenges);
}

class ChallengesEmpty extends MapState {

  ChallengesEmpty();
}

class PositionLoaded extends MapState {
  final LatLng latLng;

  PositionLoaded(this.latLng);
}

class PlacesChanged extends MapState {
  final List<Place> places;

  PlacesChanged(this.places);
}