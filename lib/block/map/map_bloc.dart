import 'package:bloc/bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';

import '../../domain/interactor/interactor.dart';
import '../../domain/model/challenge.dart';
import '../../domain/model/place.dart';
import '../../domain/model/request.dart';
import 'package:rxdart/rxdart.dart';

part 'map_event.dart';

part 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  final Interactor _interactor;

  MapBloc(this._interactor) : super(MapInitial()) {
    on<LoadChallenges>((event, emit) async {
      await emit.forEach(_interactor.registerSettingsChange(),
          onData: (challenges) {
        if (challenges.isEmpty) {
          return ChallengesEmpty();
        } else {
          return ChallengesLoaded(challenges);
        }
      });
    });
    on<UploadChallenge>((event, emit) {
      _interactor.addChallenge(event.newChallenge);
    });
    on<UploadRequest>((event, emit) {
      _interactor.addRequest(event.newRequest);
    });
    on<RegisterCurrentPosition>((event, emit) async {
      await emit.forEach(
        _interactor.getCurrentPosition(),
        onData: (position) => PositionLoaded(position),
      );
    });
    on<SearchPlaces>(
      (event, emit) async {
        final places = await _interactor.getPlaces(event.input, event.lang);
        emit(PlacesChanged(places));
      },
      transformer: (events, mapper) => events
          .debounceTime(const Duration(milliseconds: 500))
          .switchMap(mapper),
    );
  }
}
