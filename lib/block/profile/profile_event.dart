part of 'profile_bloc.dart';

@immutable
abstract class ProfileEvent {}

class LoadProfileEvent extends ProfileEvent{
  final String email;

  LoadProfileEvent(this.email);

}

class PickedFileEvent extends ProfileEvent{

}

class UploadFileEvent extends ProfileEvent{
  final PlatformFile? pickedFile;

  UploadFileEvent(this.pickedFile);
}

