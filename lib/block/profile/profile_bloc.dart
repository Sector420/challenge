import 'package:bloc/bloc.dart';
import 'package:challenge_app/domain/model/challenge_user.dart';
import 'package:file_picker/file_picker.dart';
import 'package:meta/meta.dart';

import '../../domain/interactor/interactor.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final Interactor _interactor;
  var id = "id";
  var email = "email";

  ProfileBloc(this._interactor) : super(ProfileInitialState()) {
    on<LoadProfileEvent>((event, emit) async {
      email = event.email;
      id = await _interactor.getIdByEmail(event.email);
        try {
          var profile = await _interactor.getProfileByEmail(email);
          emit(ProfileLoadedState(profile));
        } catch (e) {
          emit(ProfileErrorEventState(e.toString()));
        }
      }
    );
    on<PickedFileEvent>((event, emit) async {
        try {
          final result = await FilePicker.platform.pickFiles();
          if (result == null) {
            var profile = await _interactor.getProfileByEmail(email);
            emit(ProfileLoadedState(profile));
          }
          else {
            emit(ProfileFileSelected(result));
          }
        } catch (e) {
          emit(ProfileErrorEventState(e.toString()));
        }
      }
    );
    on<UploadFileEvent>((event, emit) async {
      try {
        final path = event.pickedFile!.path!;
        final name = event.pickedFile!.name;
        //ezen még lehet gondolkodni
        await _interactor.uploadFile(path, name, id);
        var profile = await _interactor.getProfileByEmail(email);
        emit(ProfileLoadedState(profile));
      } catch (e) {
        emit(ProfileErrorEventState(e.toString()));
      }
    }
    );
  }
}
