part of 'profile_bloc.dart';

@immutable
abstract class ProfileState{}

class ProfileInitialState extends ProfileState {

}

class ProfileLoadedState extends ProfileState {
  final ChallengeUser? user;

  ProfileLoadedState(this.user);
}

class ProfileFileSelected extends ProfileState {
  final FilePickerResult? result;

  ProfileFileSelected(this.result);
}

class ProfileErrorEventState extends ProfileState {
  final String message;

  ProfileErrorEventState(this.message);
}



