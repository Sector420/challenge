part of 'searches_bloc.dart';

@immutable
abstract class SearchesState {}

class SearchesInitial extends SearchesState {}

class SearchesLoaded extends SearchesState {
  final List<Search> searches;

  SearchesLoaded(this.searches);
}