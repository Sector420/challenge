import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../domain/interactor/interactor.dart';
import '../../domain/model/request.dart';
import '../../domain/model/search.dart';
import '../../domain/model/search_filter.dart';
import '../../domain/model/team.dart';

part 'searches_event.dart';

part 'searches_state.dart';

class SearchesBloc extends Bloc<SearchesEvent, SearchesState> {
  final Interactor _interactor;
  Stream<List<Search>>? function;
  StreamSubscription<List<Search>>? streamSubscription;

  SearchesBloc(this._interactor) : super(SearchesInitial()) {
    on<LoadSearches>((event, emit) async {
      await emit.forEach(
        _interactor.registerFilterChange(),
        onData: (searches) {
          return SearchesLoaded(searches);
        },
      );
    });
    on<UploadRequest>((event, emit) {
      _interactor.addRequest(event.newRequest);
    });
    on<UploadTeam>((event, emit) {
      _interactor.addTeam(event.newTeam);
    });
    on<UpdateRequestState>((event, emit) async {
      _interactor.updateRequestState(
          event.requestId, event.challengeId, event.state);
    });
    on<JoinTeam>((event, emit) async {
      _interactor.joinTeam(event.teamId);
    });
    on<UpdateUserRole>((event, emit) async {
      _interactor.updateUserRole(event.userId, event.role);
    });
  }
}
