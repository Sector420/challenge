part of 'searches_bloc.dart';

@immutable
abstract class SearchesEvent {}

class LoadSearches extends SearchesEvent {
  final SearchFilterEnum searchFilterEnum;

  LoadSearches(this.searchFilterEnum);
}

class UploadRequest extends SearchesEvent {
  final Request newRequest;

  UploadRequest(this.newRequest);
}

class UploadTeam extends SearchesEvent {
  final Team newTeam;

  UploadTeam(this.newTeam);
}

class ChangeFilter extends SearchesEvent {
  final SearchFilterEnum filter;

  ChangeFilter(this.filter);
}

class UpdateRequestState extends SearchesEvent {
  final String requestId;
  final String challengeId;
  final String state;

  UpdateRequestState(this.requestId, this.challengeId, this.state);
}

class JoinTeam extends SearchesEvent {
  final String teamId;

  JoinTeam(this.teamId);
}

class UpdateUserRole extends SearchesEvent {
  final String userId;
  final String role;

  UpdateUserRole(this.userId, this.role);
}
