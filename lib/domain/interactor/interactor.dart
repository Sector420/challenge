import 'dart:async';

import 'package:challenge_app/database/data_sources/challenges_ds.dart';
import 'package:challenge_app/database/data_sources/messages_ds.dart';
import 'package:challenge_app/domain/model/message.dart';
import 'package:challenge_app/domain/model/request.dart';
import 'package:challenge_app/domain/model/search.dart';
import 'package:get_it/get_it.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:rxdart/rxdart.dart';

import '../../database/data_sources/requests_ds.dart';
import '../../database/data_sources/teams_ds.dart';
import '../../database/data_sources/users_ds.dart';
import '../../database/place_api_service.dart';
import '../../database/positioning_service.dart';
import '../model/challenge.dart';
import '../model/challenge_user.dart';
import '../model/place.dart';
import '../model/search_filter.dart';
import '../model/team.dart';
import '../model/user_settings.dart';

class Interactor {
  final SearchFilter _searchFilter = GetIt.instance.get<SearchFilter>();
  final PlaceApiService _placeApiService =
  GetIt.instance.get<PlaceApiService>();

  final TeamsDataSource _teamsDataSource;
  final ChallengesDataSource _challengesDataSource;
  final UsersDataSource _usersDataSource;
  final MessagesDataSource _messagesDataSource;
  final RequestsDataSource _requestsDataSource;
  final Box<SearchFilter> _filterBox;
  final Box<UserSettings> _settingsBox;
  final UserSettings _userSettings;

  Interactor(
    this._teamsDataSource,
    this._challengesDataSource,
    this._usersDataSource,
    this._messagesDataSource,
    this._requestsDataSource,
    this._filterBox,
    this._settingsBox,
    this._userSettings,
  );

  Stream<List<Team>> getTeams() {
    return _teamsDataSource.getTeams();
  }

  void addTeam(Team newTeam) {
    _teamsDataSource.addTeam(newTeam);
  }

  void joinTeam(String teamId) {
    _teamsDataSource.joinTeam(teamId);
  }

  Stream<List<Challenge>> getChallenges() {
    return _challengesDataSource.getChallenges();
  }

  Stream<List<Challenge>> getDifferentChallenges() {
    return Rx.combineLatest(
      [
        if (_userSettings.easyLevel) _challengesDataSource.getEasyChallenges(),
        if (_userSettings.normalLevel)
          _challengesDataSource.getNormalChallenges(),
        if (_userSettings.hardLevel) _challengesDataSource.getHardChallenges(),
      ],
      (values) =>
          values.fold([], (previousValue, element) => previousValue + element),
    );
  }

  Stream<List<Search>> getSearches() {
    return Rx.merge(
      [
        if (_searchFilter.searchFilterEnum == SearchFilterEnum.challenges)
          getDifferentChallenges(),
        if (_searchFilter.searchFilterEnum == SearchFilterEnum.teams)
          getTeams(),
        if (_searchFilter.searchFilterEnum == SearchFilterEnum.players)
          getUsers(),
        if (_searchFilter.searchFilterEnum == SearchFilterEnum.requests)
          getRequests(),
      ],
    );
  }

  Stream<List<Search>> registerFilterChange() {
    return getSearchFilterChange().switchMap((_) {
      return getSearches();
    });
  }

  Stream<List<Challenge>> registerSettingsChange() {
    return getSettingsChange().switchMap((_) {
      return getDifferentChallenges();
    });
  }

  Stream<LatLng> getCurrentPosition() {
    final PositioningService positioningService =
        GetIt.instance.get<PositioningService>();
    return Stream<void>.periodic(const Duration(milliseconds: 1000))
        .asyncMap((_) async => await positioningService.getCurrentPosition())
        .where((_) => _userSettings.showPosition);
  }

  void addChallenge(Challenge newChallenge) {
    _challengesDataSource.addChallenge(newChallenge);
  }

  Stream<List<ChallengeUser>> getUsers() {
    return _usersDataSource.getUsers();
  }

  void updateUserRole(String userId, String role) {
    return _usersDataSource.updateUserRole(userId, role);
  }

  Stream<List<Message>> getMessages(String teamId) {
    return _messagesDataSource.getMessages(teamId);
  }

  void addMessage(String teamId, Message newMessage) {
    _messagesDataSource.addMessage(teamId, newMessage);
  }

  Stream<List<Request>> getRequests() {
    return _requestsDataSource.getRequests();
  }

  void addRequest(Request newRequest) {
    _requestsDataSource.addRequest(newRequest);
  }

  void updateRequestState(String requestId, String challengeId, String state) {
    _challengesDataSource.updateRequestState(requestId, challengeId, state);
  }

  Future<String> getIdByEmail(String email) async {
    return await _usersDataSource.getIdByEmail(email);
  }

  Future<ChallengeUser?> getProfileByEmail(String email) async {
    return await _usersDataSource.getProfileByEmail(email);
  }

  Future<void> uploadFile(String filePath, String fileName, String id) async {
    return await _usersDataSource.uploadFile(filePath, fileName, id);
  }

  Future<String> uploadPicture(String filePath, String fileName) async {
    return await _messagesDataSource.uploadPicture(filePath, fileName);
  }

  Stream<dynamic> getSearchFilterChange() async* {
    yield null;
    yield* _filterBox.watch();
  }

  Stream<dynamic> getSettingsChange() async* {
    yield null;
    yield* _settingsBox.watch();
  }

  Future<List<Place>> getPlaces(String input, String lang) async {
    return await _placeApiService.getPlaces(input, lang);
  }
}
