// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'challenge.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Challenge _$ChallengeFromJson(Map<String, dynamic> json) {
  return Challenge(
    id: json['id'] as String?,
    author: json['author'] as String,
    title: json['title'] as String,
    date: DateTime.parse(json['date'] as String),
    body: json['body'] as String,
    latLng: const GeoPointConverter().fromJson(json['latLng'] as GeoPoint),
    level: _$enumDecode(_$LevelEnumMap, json['level']),
    teamState: (json['teamState'] as List<dynamic>)
        .map((e) => Map<String, String>.from(e as Map))
        .toList(),
  );
}

Map<String, dynamic> _$ChallengeToJson(Challenge instance) => <String, dynamic>{
      'id': instance.id,
      'author': instance.author,
      'title': instance.title,
      'date': instance.date.toIso8601String(),
      'body': instance.body,
      'latLng': const GeoPointConverter().toJson(instance.latLng),
      'level': _$LevelEnumMap[instance.level],
      'teamState': instance.teamState,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$LevelEnumMap = {
  Level.easy: 'easy',
  Level.normal: 'normal',
  Level.hard: 'hard',
};
