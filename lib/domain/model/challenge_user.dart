import 'package:challenge_app/domain/model/search.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../view/item/challenge_user_item.dart';

part 'challenge_user.g.dart';

@JsonSerializable()
class ChallengeUser extends Search {
  final String? id;
  final String username;
  final String email;
  final String imageUrl;
  final String role;
  final String? teamId;
  final int point;

  ChallengeUser({
    this.id,
    required this.username,
    required this.email,
    required this.imageUrl,
    required this.role,
    this.teamId,
    required this.point,
  });

  @override
  Widget createItemWidget() {
    return ChallengeUserItem(this);
  }

  factory ChallengeUser.fromJson(dynamic json, String id){
    return ChallengeUser(
      id: id,
      username: json['username'] as String,
      email: json['email'] as String,
      imageUrl: json['imageUrl'] as String,
      role: json['role'] as String,
      teamId: json['teamId'] as String?,
      point: json['point'] as int,
    );
  }

  Map<String, dynamic> toJson() => _$ChallengeUserToJson(this);
}
