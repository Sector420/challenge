import 'package:hive/hive.dart';

part 'search_filter.g.dart';

@HiveType(typeId: 1)
class SearchFilter extends HiveObject {
  @HiveField(0)
  SearchFilterEnum searchFilterEnum = SearchFilterEnum.challenges;
}

@HiveType(typeId: 2)
enum SearchFilterEnum {
  @HiveField(0)
  challenges,

  @HiveField(1)
  teams,

  @HiveField(2)
  players,

  @HiveField(3)
  requests,
}
