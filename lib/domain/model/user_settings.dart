import 'package:hive/hive.dart';

part 'user_settings.g.dart';

@HiveType(typeId: 0)
class UserSettings extends HiveObject {
  @HiveField(0)
  bool easyLevel = true;

  @HiveField(1)
  bool normalLevel = true;

  @HiveField(2)
  bool hardLevel = true;

  @HiveField(3)
  bool showPosition = true;
}