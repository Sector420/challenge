// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_filter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SearchFilterEnumAdapter extends TypeAdapter<SearchFilterEnum> {
  @override
  final int typeId = 2;

  @override
  SearchFilterEnum read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return SearchFilterEnum.challenges;
      case 1:
        return SearchFilterEnum.teams;
      case 2:
        return SearchFilterEnum.players;
      case 3:
        return SearchFilterEnum.requests;
      default:
        return SearchFilterEnum.challenges;
    }
  }

  @override
  void write(BinaryWriter writer, SearchFilterEnum obj) {
    switch (obj) {
      case SearchFilterEnum.challenges:
        writer.writeByte(0);
        break;
      case SearchFilterEnum.teams:
        writer.writeByte(1);
        break;
      case SearchFilterEnum.players:
        writer.writeByte(2);
        break;
      case SearchFilterEnum.requests:
        writer.writeByte(3);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SearchFilterEnumAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class SearchFilterAdapter extends TypeAdapter<SearchFilter> {
  @override
  final int typeId = 1;

  @override
  SearchFilter read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SearchFilter()..searchFilterEnum = fields[0] as SearchFilterEnum;
  }

  @override
  void write(BinaryWriter writer, SearchFilter obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.searchFilterEnum);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SearchFilterAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
