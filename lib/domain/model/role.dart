enum Role {
  unknown('unknown'),
  player('player'),
  manager('manager'),
  admin('admin');

  final String hungarianDisplayName;

  const Role(this.hungarianDisplayName);

  static Role fromString(String string) {
    for (var role in Role.values) {
      if (role.toString().toLowerCase().split(".")[1] == string.toLowerCase()) {
        return role;
      }
    }
    throw ArgumentError("Parsing error: cannot parse $string");
  }
}