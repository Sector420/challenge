class Place {
  final String placeId;
  final String description;

  Place(this.placeId, this.description);
}