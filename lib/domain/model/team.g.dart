// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Team _$TeamFromJson(Map<String, dynamic> json) {
  return Team(
    author: json['author'] as String,
    title: json['title'] as String,
    point: json['point'] as int,
  );
}

Map<String, dynamic> _$TeamToJson(Team instance) => <String, dynamic>{
      'author': instance.author,
      'title': instance.title,
      'point': instance.point,
    };
