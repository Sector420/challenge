// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Request _$RequestFromJson(Map<String, dynamic> json) {
  return Request(
    id: json['id'] as String?,
    text: json['text'] as String,
    date: DateTime.parse(json['date'] as String),
    author: json['author'] as String,
    imageUrl: json['imageUrl'] as String?,
    teamId: json['teamId'] as String,
    userId: json['userId'] as String,
    challengeId: json['challengeId'] as String,
    type: json['type'] as String,
  );
}

Map<String, dynamic> _$RequestToJson(Request instance) => <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'date': instance.date.toIso8601String(),
      'author': instance.author,
      'imageUrl': instance.imageUrl,
      'teamId': instance.teamId,
      'userId': instance.userId,
      'challengeId': instance.challengeId,
      'type': instance.type,
    };
