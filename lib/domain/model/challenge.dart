import 'package:challenge_app/domain/model/search.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../view/item/challenge_item.dart';

part 'challenge.g.dart';

@JsonSerializable()
class Challenge extends Search {

  final String? id;
  final String author;
  final String title;
  final DateTime date;
  final String body;
  @GeoPointConverter()
  GeoPoint latLng;
  final Level level;
  final List<Map<String, String>> teamState;

  Challenge({
    this.id,
    required this.author,
    required this.title,
    required this.date,
    required this.body,
    required this.latLng,
    required this.level,
    required this.teamState,
  });

  @override
  Widget createItemWidget() {
    return ChallengeItem(this);
  }

  factory Challenge.fromJson(dynamic json, String id){
    return Challenge(
      id: id,
      author: json['author'] as String,
      title: json['title'] as String,
      date: DateTime.parse(json['date'] as String),
      body: json['body'] as String,
      latLng: const GeoPointConverter().fromJson(json['latLng'] as GeoPoint),
      level: _$enumDecode(_$LevelEnumMap, json['level']),
      teamState: (json['teamState'] as List<dynamic>)
          .map((e) => Map<String, String>.from(e as Map))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() => _$ChallengeToJson(this);
}

class GeoPointConverter
    implements JsonConverter<GeoPoint, GeoPoint> {
  const GeoPointConverter();

  @override
  GeoPoint fromJson(GeoPoint geoPoint) {
    return geoPoint;
  }

  @override
  GeoPoint toJson(GeoPoint geoPoint) =>
      geoPoint;
}

enum Level {
  @JsonValue('easy')
  easy,

  @JsonValue('normal')
  normal,

  @JsonValue('hard')
  hard,
}