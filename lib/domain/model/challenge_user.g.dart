// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'challenge_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Map<String, dynamic> _$ChallengeUserToJson(ChallengeUser instance) =>
    <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'email': instance.email,
      'imageUrl': instance.imageUrl,
      'role': instance.role,
      'teamId': instance.teamId,
      'point': instance.point,
    };
