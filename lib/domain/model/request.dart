import 'package:challenge_app/domain/model/search.dart';
import 'package:challenge_app/view/item/request_item.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'request.g.dart';

@JsonSerializable()
class Request extends Search {
  final String? id;
  final String text;
  final DateTime date;
  final String author;
  final String? imageUrl;
  final String teamId;
  final String userId;
  final String challengeId;
  final String type;

  Request({
    this.id,
    required this.text,
    required this.date,
    required this.author,
    this.imageUrl,
    required this.teamId,
    required this.userId,
    required this.challengeId,
    required this.type,
  });

  @override
  Widget createItemWidget() {
    return RequestItem(this);
  }

  factory Request.fromJson(dynamic json, String id){
    return Request(
      id: id,
      text: json['text'] as String,
      date: DateTime.parse(json['date'] as String),
      author: json['author'] as String,
      imageUrl: json['imageUrl'] as String?,
      teamId: json['teamId'] as String,
      userId: json['userId'] as String,
      challengeId: json['challengeId'] as String,
      type: json['type'] as String,
    );
  }

  Map<String, dynamic> toJson() => _$RequestToJson(this);
}

enum RequestState {
  @JsonValue('pending')
  pending,

  @JsonValue('denied')
  denied,

  @JsonValue('accepted')
  accepted,
}
