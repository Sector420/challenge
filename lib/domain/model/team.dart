import 'package:challenge_app/domain/model/search.dart';
import 'package:challenge_app/view/item/team_item.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'team.g.dart';

@JsonSerializable()
class Team extends Search {
  final String author;
  final String title;
  final int point;

  Team({
    required this.author,
    required this.title,
    required this.point,
  });

  @override
  Widget createItemWidget() {
    return TeamItem(this);
  }

  factory Team.fromJson(dynamic json) => _$TeamFromJson(json);

  Map<String, dynamic> toJson() => _$TeamToJson(this);
}
