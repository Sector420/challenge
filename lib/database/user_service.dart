import 'package:challenge_app/domain/model/challenge_user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../domain/model/role.dart';

class UserService {
  late Role role;

  late ChallengeUser currentUser;

  late String id;

  Future<bool> init() async {
    final users = FirebaseFirestore.instance.collection('users');
    final user = FirebaseAuth.instance.currentUser;
    final snapshot = await users.doc(user!.uid).get();
    id = snapshot.id;
    currentUser = ChallengeUser.fromJson(snapshot.data(), snapshot.id);
    role = Role.fromString(currentUser.role);

    return true;
  }
}
