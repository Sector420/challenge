import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

abstract class FBApi {
  CollectionReference<Map<String, dynamic>> getTeams();
  CollectionReference<Map<String, dynamic>> getChallenges();
  CollectionReference<Map<String, dynamic>> getUsers();
  CollectionReference<Map<String, dynamic>> getMessages(String teamId);
  CollectionReference<Map<String, dynamic>> getRequests();
  FirebaseStorage getStorage();
}