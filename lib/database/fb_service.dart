import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'fb_api.dart';


class FBService implements FBApi {

  @override
  CollectionReference<Map<String, dynamic>> getTeams() {
    return FirebaseFirestore.instance.collection('teams');
  }

  @override
  CollectionReference<Map<String, dynamic>> getChallenges() {
    return FirebaseFirestore.instance.collection('challenges');
  }

  @override
  CollectionReference<Map<String, dynamic>> getUsers() {
    return FirebaseFirestore.instance.collection('users');
  }

  @override
  CollectionReference<Map<String, dynamic>> getMessages(String teamId) {
    return FirebaseFirestore.instance.collection('teams').doc(teamId).collection('messages');
  }

  @override
  CollectionReference<Map<String, dynamic>> getRequests() {
    return FirebaseFirestore.instance.collection('requests');
  }

  @override
  FirebaseStorage getStorage() {
    return FirebaseStorage.instance;
  }
}