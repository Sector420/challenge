import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../domain/model/place.dart';


class PlaceApiService {
  final String apiKey = 'AIzaSyCBu4EV6ieIa8EurKOHbLIrSG24NOE4NK8';

  Future<List<Place>> getPlaces(String input, String lang) async {
    final request =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=address&language=$lang&components=country:hun&key=$apiKey';
    final response = await http.get(Uri.parse(request));

    if (response.statusCode == 200) {
      final result = convert.json.decode(response.body);
      if (result['status'] == 'OK') {
        return result['predictions']
            .map<Place>((p) => Place(p['place_id'], p['description']))
            .toList();
      }
      if (result['status'] == 'ZERO_RESULTS') {
        return [];
      }
      return [];
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  Future<LatLng> getPlaceDetailFromId(String placeId) async {
    final request =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&key=$apiKey';
    final response = await http.get(Uri.parse(request));

    final result = convert.jsonDecode(response.body);

    final lat = result['result']['geometry']['location']['lat'];
    final lng = result['result']['geometry']['location']['lng'];

    return LatLng(lat, lng);
  }
}