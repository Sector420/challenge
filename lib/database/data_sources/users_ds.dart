import 'dart:io';

import '../../domain/model/challenge_user.dart';
import '../fb_api.dart';

class UsersDataSource {
  final FBApi _fbApi;

  UsersDataSource(this._fbApi);

  Stream<List<ChallengeUser>> getUsers() {
    return _fbApi.getUsers().snapshots().map((event) =>
        event.docs.map((e) => ChallengeUser.fromJson(e.data(), e.id)).toList());
  }

  void updateUserRole(String userId, String role) {
    _fbApi.getUsers().doc(userId).update({'role': role});
    if(role == 'manager') {
      _fbApi.getUsers().doc(userId).update({'teamId' : 'eBAm3T3GGZSzPR0Hx2ot'});
    }
  }

  Future<String> getIdByEmail(String email) async {
    final snapshot =
        await _fbApi.getUsers().where('email', isEqualTo: email).get();
    return snapshot.docs.first.id;
  }

  Future<ChallengeUser> getProfileByEmail(String email) async {
    var snapshot =
        await _fbApi.getUsers().where('email', isEqualTo: email).get();
    final profile = snapshot.docs
        .map((doc) => ChallengeUser.fromJson(doc.data(), doc.id))
        .toList()
        .first;
    return profile;
  }

  Future<void> uploadFile(String filePath, String fileName, String id) async {
    File file = File(filePath);
    await _fbApi.getStorage().ref('test/$fileName').putFile(file);
    final path = 'test/$fileName';
    final imageURl =
        await _fbApi.getStorage().ref().child(path).getDownloadURL();
    await _fbApi.getUsers().doc(id).update({'imageUrl': imageURl});
  }
}
