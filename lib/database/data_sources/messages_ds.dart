import 'dart:io';

import '../../domain/model/message.dart';
import '../fb_api.dart';

class MessagesDataSource {
  final FBApi _fbApi;

  MessagesDataSource(this._fbApi);

  Stream<List<Message>> getMessages(String teamId) {
    return _fbApi
        .getMessages(teamId)
        .orderBy('date', descending: true)
        .snapshots()
        .map((event) =>
            event.docs.map((e) => Message.fromJson(e.data())).toList());
  }

  void addMessage(String teamId, Message newMessage) {
    _fbApi.getMessages(teamId).add(newMessage.toJson());
  }

  Future<String> uploadPicture(String filePath, String fileName) async {
    File file = File(filePath);
    await _fbApi.getStorage().ref('messages/$fileName').putFile(file);
    final path = 'messages/$fileName';
    final imageURl =
        await _fbApi.getStorage().ref().child(path).getDownloadURL();
    return imageURl;
  }
}
