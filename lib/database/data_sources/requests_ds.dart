import 'package:challenge_app/domain/model/request.dart';

import '../fb_api.dart';

class RequestsDataSource {
  final FBApi _fbApi;

  RequestsDataSource(this._fbApi);

  Stream<List<Request>> getRequests() {
    return _fbApi.getRequests().snapshots().map(
        (event) => event.docs.map((e) => Request.fromJson(e.data(), e.id)).toList());
  }

  void addRequest(Request newRequest) async {
    final snapshot = await _fbApi
        .getRequests()
        .where('challengeId', isEqualTo: newRequest.challengeId)
        .where('teamId', isEqualTo: newRequest.teamId)
        .get();

    if(snapshot.docs.isNotEmpty) {
      _fbApi.getRequests().doc(snapshot.docs.first.id).update(newRequest.toJson());
    }
    else {
      _fbApi.getRequests().add(newRequest.toJson());
    }
  }
}
