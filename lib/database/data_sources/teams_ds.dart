import 'package:get_it/get_it.dart';

import '../../domain/model/team.dart';
import '../fb_api.dart';
import '../user_service.dart';

class TeamsDataSource {
  final UserService _userService = GetIt.instance.get<UserService>();
  final FBApi _fbApi;

  TeamsDataSource(this._fbApi);

  Stream<List<Team>> getTeams() {
    return _fbApi.getTeams().snapshots().map(
            (event) => event.docs.map((e) => Team.fromJson(e.data())).toList());
  }

  void addTeam(Team newTeam) {
    _fbApi.getTeams().add(newTeam.toJson());
  }

  void joinTeam(String id) async {
    final snapshot = await _fbApi.getTeams().get();
    final teamsIds = snapshot.docs.map((e) => e.id).toList();
    if(teamsIds.contains(id)) {
      _fbApi.getUsers().doc(_userService.id).update({'teamId':id});
      _fbApi.getUsers().doc(_userService.id).update({'role':'player'});
    }
  }
}
