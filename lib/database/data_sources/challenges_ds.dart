import 'package:challenge_app/domain/model/challenge.dart';

import '../fb_api.dart';

class ChallengesDataSource {
  final FBApi _fbApi;

  ChallengesDataSource(this._fbApi);

  Stream<List<Challenge>> getChallenges() {
    return _fbApi.getChallenges().snapshots().map((event) =>
        event.docs.map((e) => Challenge.fromJson(e.data(), e.id)).toList());
  }

  Stream<List<Challenge>> getEasyChallenges() {
    return _fbApi
        .getChallenges()
        .where('level', isEqualTo: 'easy')
        .snapshots()
        .map((event) =>
            event.docs.map((e) => Challenge.fromJson(e.data(), e.id)).toList());
  }

  Stream<List<Challenge>> getNormalChallenges() {
    return _fbApi
        .getChallenges()
        .where('level', isEqualTo: 'normal')
        .snapshots()
        .map((event) =>
            event.docs.map((e) => Challenge.fromJson(e.data(), e.id)).toList());
  }

  Stream<List<Challenge>> getHardChallenges() {
    return _fbApi
        .getChallenges()
        .where('level', isEqualTo: 'hard')
        .snapshots()
        .map((event) =>
            event.docs.map((e) => Challenge.fromJson(e.data(), e.id)).toList());
  }

  void addChallenge(Challenge newChallenge) {
    _fbApi.getChallenges().add(newChallenge.toJson());
  }

  void removeChallenge(String challengeId) {
    _fbApi.getChallenges().doc(challengeId).delete();
  }

  void updateRequestState(String requestId, String challengeId, String state) async {
    final snapshot = await _fbApi.getChallenges().doc(challengeId).get();
    Challenge challenge = Challenge.fromJson(snapshot.data(), snapshot.id);
    Map<String, String> request = challenge.teamState.firstWhere((element) => element['requestId'] == requestId);
    challenge.teamState.remove(request);
    request['state'] = state;
    challenge.teamState.add(request);
    _fbApi.getChallenges().doc(challengeId).update(challenge.toJson());
  }
}
