import 'package:challenge_app/view/page/profile_area.dart';
import 'package:challenge_app/view/page/settings_area.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../database/user_service.dart';
import '../../domain/model/user_settings.dart';

class Options extends StatefulWidget {
  const Options({Key? key}) : super(key: key);

  @override
  State<Options> createState() => _OptionsState();
}

class _OptionsState extends State<Options> {
  final user = FirebaseAuth.instance.currentUser;
  final UserSettings _userSettings = GetIt.instance.get<UserSettings>();
  final UserService _userService = GetIt.instance.get<UserService>();

  String url =
      'https://firebasestorage.googleapis.com/v0/b/challenge-363613.appspot.com/o/Question_mark.png?alt=media&token=580a2ae9-f54c-4859-a2c5-3a44ded11a45';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Options')),
      body: ListView(
        padding: const EdgeInsets.all(10),
        children: [
          ProfileArea(),
          SettingsArea(
            areaName: 'Level',
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    child: Text(
                      'Easy',
                      softWrap: true,
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Switch(
                      activeColor: Colors.blue,
                      value: _userSettings.easyLevel,
                      onChanged: (bool value) async {
                        setState(() {
                          _userSettings.easyLevel = value;
                        });
                        await _userSettings.save();
                      },
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    child: Text(
                      'Normal',
                      softWrap: true,
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Switch(
                      activeColor: Colors.blue,
                      value: _userSettings.normalLevel,
                      onChanged: (bool value) async {
                        setState(() {
                          _userSettings.normalLevel = value;
                        });
                        await _userSettings.save();
                      },
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    child: Text(
                      'Hard',
                      softWrap: true,
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Switch(
                      activeColor: Colors.blue,
                      value: _userSettings.hardLevel,
                      onChanged: (bool value) async {
                        setState(() {
                          _userSettings.hardLevel = value;
                        });
                        await _userSettings.save();
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
          SettingsArea(
            areaName: 'Position',
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    child: Text(
                      'Show current position',
                      softWrap: true,
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Switch(
                      activeColor: Colors.blue,
                      value: _userSettings.showPosition,
                      onChanged: (bool value) async {
                        setState(() {
                          _userSettings.showPosition = value;
                        });
                        await _userSettings.save();
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: () {
              _sendEmail('Share event', _userService.currentUser.teamId!);
            },
            child: const Text('Send invite'),
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: () {
              _logOut();
            },
            child: const Text('Sing out'),
          ),
        ],
      ),
    );
  }

  Future<void> _logOut() {
    return FirebaseAuth.instance.signOut();
  }

  void _sendEmail(String subject, String body) {
    final url =
        'mailto:?subject=${Uri.encodeFull(subject)}&body=${Uri.encodeFull(body)}';
    launch(url);
  }
}
