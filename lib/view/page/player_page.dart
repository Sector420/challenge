import 'package:challenge_app/view/page/chat_page.dart';
import 'package:challenge_app/view/page/search_page.dart';
import 'package:challenge_app/view/page/map_page_stream.dart';
import 'package:challenge_app/view/page/mape_page_controller.dart';
import 'package:get_it/get_it.dart';

import '../../database/user_service.dart';

import '../../domain/model/role.dart';

import 'package:flutter/material.dart';

import 'options.dart';

class PlayerPage extends StatefulWidget {
  const PlayerPage({super.key});

  @override
  State<PlayerPage> createState() => _PlayerPageState();
}

class _PlayerPageState extends State<PlayerPage>
    with SingleTickerProviderStateMixin {
  final MapPageController _playerPageController =
      GetIt.instance.get<MapPageController>();
  final UserService _userService = GetIt.instance.get<UserService>();

  late final TabController _tabController;

  @override
  void initState() {
    super.initState();
    final int tabNum;
    final int initialIndex;
    switch (_userService.role) {
      case Role.unknown:
        tabNum = 1;
        initialIndex = 0;
        break;
      case Role.player:
        tabNum = 4;
        initialIndex = 1;
        break;
      case Role.manager:
        tabNum = 4;
        initialIndex = 1;
        break;
      case Role.admin:
        tabNum = 4;
        initialIndex = 1;
        break;
    }
    _tabController =
        TabController(length: tabNum, vsync: this, initialIndex: initialIndex);
    _tabController.addListener(() => setState(() {}));

    _playerPageController.tabController = _tabController;
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: TabBar(
        controller: _tabController,
        unselectedLabelColor: Colors.black,
        labelColor: Colors.white,
        indicator: const BoxDecoration(
          color: Colors.lightBlueAccent,
        ),
        tabs: [
          const Tab(icon: Icon(Icons.map), text: 'Search'),
          if (_userService.role != Role.unknown)
            const Tab(icon: Icon(Icons.videogame_asset_sharp), text: 'Map'),
          if (_userService.role != Role.unknown)
            const Tab(icon: Icon(Icons.chat), text: 'Chat'),
          if (_userService.role != Role.unknown)
            const Tab(icon: Icon(Icons.keyboard_option_key), text: 'Options')
        ],
      ),
      body: TabBarView(
        physics: const NeverScrollableScrollPhysics(),
        controller: _tabController,
        children: [
          SearchPage(),
          if (_userService.role != Role.unknown)
            MapPageStream(
              latLng: _playerPageController.currentLatLng,
            ),
          if (_userService.role != Role.unknown) ChatPage(),
          if (_userService.role != Role.unknown) const Options(),
        ],
      ),
    );
  }
}
