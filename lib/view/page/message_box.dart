import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import '../../block/messages/messages_bloc.dart';
import '../../database/user_service.dart';
import '../../di/di_utils.dart';
import '../../domain/interactor/interactor.dart';
import '../../domain/model/message.dart';

class MessageBox extends StatelessWidget {
  final UserService _userService = GetIt.instance.get<UserService>();

  final TextEditingController controller = TextEditingController();

  final user = FirebaseAuth.instance.currentUser;
  final PlatformFile? pickedFile;

  MessageBox({super.key, required this.pickedFile});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      color: Colors.grey.shade200,
      child: Row(
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (pickedFile == null)
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: TextFormField(
                        controller: controller,
                        decoration: InputDecoration(
                          fillColor: Colors.yellow,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          contentPadding: const EdgeInsets.all(12),
                          hintText: 'Type your message here...',
                        ),
                      ),
                    ),
                  ),
                if (pickedFile != null) ...[
                  Image(
                    height: 100,
                    width: 100,
                    image: FileImage(File(pickedFile!.path!)),
                  ),
                  IconButton(
                    icon: const Icon(Icons.delete),
                    onPressed: () async {
                      context.read<MessagesBloc>().add(DeletePicture());
                    },
                  ),
                ],
              ],
            ),
          ),
          IconButton(
            icon: const Icon(Icons.add_a_photo),
            onPressed: () async {
              context.read<MessagesBloc>().add(FilePickedEvent());
            },
          ),
          IconButton(
            icon: const Icon(Icons.send),
            onPressed: () async {
              final messagesBloc = context.read<MessagesBloc>();
              String? imageUrl;
              if (pickedFile != null) {
                imageUrl = await injector<Interactor>()
                    .uploadPicture(pickedFile!.path!, pickedFile!.name);
              }
              if (controller.text.isNotEmpty || pickedFile != null) {
                Message message = Message(
                  text: controller.text,
                  date: DateTime.now(),
                  author: user!.email!,
                  imageUrl: imageUrl,
                );
                messagesBloc.add(
                    UploadMessage(_userService.currentUser.teamId!, message));
                controller.clear();
              }
            },
          ),
        ],
      ),
    );
  }
}
