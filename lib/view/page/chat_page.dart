import 'package:challenge_app/view/page/pictures_page.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';

import '../../block/messages/messages_bloc.dart';
import '../../database/user_service.dart';
import '../../di/di_utils.dart';
import '../../domain/model/message.dart';
import '../item/message_item.dart';
import 'message_box.dart';

class ChatPage extends StatelessWidget {
  final UserService _userService = GetIt.instance.get<UserService>();

  final user = FirebaseAuth.instance.currentUser;

  final List<String> urlList = [];

  ChatPage({super.key});

  @override
  Widget build(BuildContext context) {
    PlatformFile? pickedFile;
    List<Message> messages = [];
    return BlocProvider(
      create: (context) =>
          injector<MessagesBloc>()..add(LoadMessages(_userService.currentUser.teamId!)),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Chat'),
          actions: [
            InkWell(
              child: const Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: Icon(Icons.photo_library)),
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) => PicturesPage(urlList: urlList),
                  ),
                );
              },
            ),
          ],
        ),
        body: BlocBuilder<MessagesBloc, MessagesState>(
          builder: (context, state) {
            if (state is MessagesInitial) {
              return const SizedBox(
                height: 100,
                width: 100,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            if (state is FileSelected) {
              pickedFile = state.result!.files.first;
            } else if (state is MessagesLoaded) {
              messages = state.messages;
            } else if (state is PictureDisable) {
              pickedFile = null;
            }
            return Column(
              children: [
                Expanded(
                  child: GroupedListView<Message, DateTime>(
                    reverse: true,
                    elements: messages,
                    order: GroupedListOrder.DESC,
                    useStickyGroupSeparators: true,
                    floatingHeader: true,
                    groupBy: (message) => DateTime(
                      message.date.year,
                      message.date.month,
                      message.date.day,
                    ),
                    groupHeaderBuilder: (Message message) => SizedBox(
                      height: 40,
                      child: Center(
                        child: Card(
                          color: Theme.of(context).primaryColor,
                          child: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Text(
                              DateFormat.yMMMd().format(message.date),
                              style: const TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                    indexedItemBuilder: (context, Message message, index) {
                      var message = messages[index];
                      if (message.imageUrl != null &&
                          !urlList.contains(message.imageUrl)) {
                        urlList.add(message.imageUrl!);
                      }
                      return MessageItem(message);
                    },
                  ),
                ),
                MessageBox(pickedFile: pickedFile),
              ],
            );
          },
        ),
      ),
    );
  }
}
