import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../block/profile/profile_bloc.dart';
import '../../di/di_utils.dart';

class ProfileArea extends StatelessWidget {
  final user = FirebaseAuth.instance.currentUser;

  final String jake =
      'https://firebasestorage.googleapis.com/v0/b/challenge-363613.appspot.com/o/Question_mark.png?alt=media&token=580a2ae9-f54c-4859-a2c5-3a44ded11a45';

  final String urlke =
      'https://firebasestorage.googleapis.com/v0/b/challenge-363613.appspot.com/o/Question_mark.png?alt=media&token=580a2ae9-f54c-4859-a2c5-3a44ded11a45';

  ProfileArea({super.key});

  @override
  Widget build(BuildContext context) {
    PlatformFile? pickedFile;
    return BlocProvider(
      create: (context) =>
          injector<ProfileBloc>()..add(LoadProfileEvent(user!.email!)),
      child: Align(
        alignment: Alignment.topRight,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    BlocBuilder<ProfileBloc, ProfileState>(
                      builder: (context, state) {
                        var url = jake;
                        if (state is ProfileLoadedState) {
                          final profile = state.user;
                          profile == null ? url = jake : url = profile.imageUrl;
                          return CircleAvatar(
                            radius: 80,
                            backgroundImage: NetworkImage(url),
                          );
                        }
                        if (state is ProfileFileSelected) {
                          pickedFile = state.result!.files.first;
                          return CircleAvatar(
                            radius: 80,
                            backgroundImage: FileImage(File(pickedFile!.path!)),
                          );
                        } else {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                      },
                    ),
                    Builder(
                      builder: (context) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                              child: const Text("Select File"),
                              onPressed: () {
                                context
                                    .read<ProfileBloc>()
                                    .add(PickedFileEvent());
                              },
                            ),
                            ElevatedButton(
                              child: const Text("Upload File"),
                              onPressed: () {
                                if (pickedFile != null) {
                                  context
                                      .read<ProfileBloc>()
                                      .add(UploadFileEvent(pickedFile));
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content:
                                          Text("You didn't chose any file"),
                                    ),
                                  );
                                }
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
            BlocBuilder<ProfileBloc, ProfileState>(
              builder: (context, state) {
                if (state is ProfileLoadedState) {
                  final profile = state.user;
                  var title = "asd";
                  profile == null
                      ? title = "No Profile"
                      : title = profile.username;
                  return Column(
                    children: [
                      Text(title, style: const TextStyle(fontSize: 30)),
                      const SizedBox(height: 15),
                      Text(profile!.email,
                          style: const TextStyle(fontSize: 15)),
                    ],
                  );
                } else {
                  return const CircularProgressIndicator();
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
