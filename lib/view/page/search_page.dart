import 'package:challenge_app/di/di_utils.dart';
import 'package:challenge_app/domain/model/challenge.dart';
import 'package:challenge_app/view/dialog/create_team_dialog.dart';
import 'package:challenge_app/view/page/filter_radio_group.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import '../../block/searches/searches_bloc.dart';
import '../../database/user_service.dart';
import '../../domain/model/role.dart';
import '../../domain/model/search.dart';
import '../../domain/model/search_filter.dart';
import '../dialog/join_team_dialog.dart';

class SearchPage extends StatelessWidget {
  final UserService _userService = GetIt.instance.get<UserService>();
  final SearchFilter _searchFilter = GetIt.instance.get<SearchFilter>();

  SearchPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Search> searches = [];
    return BlocProvider(
      create: (context) =>
          injector<SearchesBloc>()..add(LoadSearches(SearchFilterEnum.challenges)),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Searches'),
        ),
        body: BlocBuilder<SearchesBloc, SearchesState>(
          builder: (context, state) {
            if (state is SearchesLoaded) {
              searches = state.searches;
            }
            return Column(
              children: [
                if (_userService.role == Role.admin)
                  ElevatedButton(
                    child: const Text('Create Team'),
                    onPressed: () async {
                      final searchesBloc = context.read<SearchesBloc>();
                      var result = await showDialog(
                        context: context,
                        builder: (BuildContext context) =>
                            const CreateTeamDialog(),
                      );
                      if (result != null) {
                        searchesBloc.add(UploadTeam(result));
                      }
                    },
                  ),
                if (_userService.role == Role.unknown)
                  Center(
                    child: ElevatedButton(
                      child: const Text('Join Team'),
                      onPressed: () async {
                        final searchesBloc = context.read<SearchesBloc>();
                        String? teamId = await showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              const JoinTeamDialog(),
                        );
                        if (teamId != null) {
                          searchesBloc.add(JoinTeam(teamId));
                        }
                      },
                    ),
                  ),
                if (_userService.role != Role.unknown)
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.black)),
                    child: const ExpansionTile(
                      title: Text(
                        'Search Options',
                        style: TextStyle(fontSize: 20),
                      ),
                      children: [
                        FilterRadioGroup(),
                      ],
                    ),
                  ),
                ),
                if (searches.isNotEmpty && _userService.role != Role.unknown)
                  Expanded(
                    child: ListView.builder(
                      itemCount: searches.length,
                      itemBuilder: (context, index) {
                        var search = searches[index];
                        return search.createItemWidget();
                      },
                    ),
                  ),
                if (searches.isEmpty) const Text('There is nothing'),
              ],
            );
          },
        ),
      ),
    );
  }
}
