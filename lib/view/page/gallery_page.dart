import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class GalleryPage extends StatelessWidget {
  final PageController pageController;
  final List<String> urlList;
  final int index;

  GalleryPage({super.key, required this.urlList, required this.index})
      : pageController = PageController(initialPage: index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PhotoViewGallery.builder(
            pageController: pageController,
            itemCount: urlList.length,
            builder: (context, index) {
              final urlImage = urlList[index];

              return PhotoViewGalleryPageOptions.customChild(
                child: Image.network(
                  urlImage,
                ),
                minScale: PhotoViewComputedScale.contained,
                maxScale: PhotoViewComputedScale.contained * 4,
              );
            },
          ),
          Positioned(
            top: 50,
            left: 30,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(shape: const StadiumBorder()),
              onPressed: () {
                Navigator.pop(context, true);
              },
              child: const Icon(Icons.arrow_back),
            ),
          ),
        ],
      ),
    );
  }
}
