import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:collection/collection.dart';

import '../../domain/model/challenge.dart';
import '../../domain/model/place.dart';

class MapPageController extends ValueNotifier<bool> {
  MapPageController() : super(false);

  TabController? tabController;

  GoogleMapController? googleMapController;

  Set<Marker> markers = {};

  addMarker(Marker marker) {
    markers.add(marker);
  }

  removeMarker(Marker marker) {
    markers.remove(marker);
  }

  createMarkers(List<Challenge> challenges) {
    markers = {};
    for (final challenge in challenges) {
      markers.add(
        Marker(
          markerId: MarkerId(challenge.title),
          infoWindow: InfoWindow(title: challenge.title),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          position:
              LatLng(challenge.latLng.latitude, challenge.latLng.longitude),
          onTap: () {
            this.challenge = challenge;
          },
        ),
      );
    }
  }

  changeCurrentLocation(LatLng position) {
    final locationMarker = markers
        .firstWhereOrNull((element) => element.markerId.value == 'location');
    if (locationMarker != null) {
      markers.remove(locationMarker);
    }
    markers.add(
      Marker(
        markerId: const MarkerId('location'),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        position: LatLng(position.latitude, position.longitude),
      ),
    );
  }

  int _tabIndex = 0;

  int get tabIndex => _tabIndex;

  set tabIndex(int value) {
    _tabIndex = value;
    tabController!.animateTo(value);
  }

  List<Place> places = [];

  LatLng _currentLatLng = const LatLng(47.49699846136928, 19.070289025527508);

  LatLng get currentLatLng => _currentLatLng;

  set currentLatLng(LatLng value) {
    _currentLatLng = value;
    googleMapController!.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(_currentLatLng.latitude, _currentLatLng.longitude),
          zoom: 16,
        ),
      ),
    );
  }

  Challenge? _challenge;

  Challenge? get challenge => _challenge;

  set challenge(Challenge? challenge) {
    _challenge = challenge;
    notifyListeners();
  }
}
