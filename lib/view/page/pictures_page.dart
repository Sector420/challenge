import 'package:flutter/material.dart';

import '../item/photo_item.dart';

class PicturesPage extends StatelessWidget {
  final List<String> urlList;

  const PicturesPage({super.key, required this.urlList});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pictures'),
      ),
      body: GridView.builder(
        itemCount: urlList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisExtent: 100,
          crossAxisCount: 3,
          mainAxisSpacing: 4,
          crossAxisSpacing: 4,
          childAspectRatio: 1 / 1,
        ),
        itemBuilder: (context, index) {
          final item = PhotoItem(index, urlList);
          return item;
        },
      ),
    );
  }
}
