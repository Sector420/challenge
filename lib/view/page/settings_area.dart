import 'package:flutter/material.dart';

class SettingsArea extends StatelessWidget {
  final String? areaName;
  final List<Widget> children;

  const SettingsArea({
    super.key,
    this.areaName,
    required this.children,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        padding: (children.last is RadioListTile) ? const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0) : const EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.black,),
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                areaName!,
                style: const TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            if (children.isNotEmpty) ...[
              const SizedBox(
                height: 10,
              ),
              ...children,
            ]
          ],
        ),
      ),
    );
  }
}
