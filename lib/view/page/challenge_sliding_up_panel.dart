import 'dart:io';

import 'package:challenge_app/block/map/map_bloc.dart';
import 'package:challenge_app/view/page/mape_page_controller.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../database/user_service.dart';
import '../../di/di_utils.dart';
import '../../domain/interactor/interactor.dart';
import '../../domain/model/challenge.dart';
import '../../domain/model/request.dart';
import '../dialog/image_dialog.dart';
import 'package:collection/collection.dart';

import '../item/challenge_item.dart';

class ChallengeSlidingUpPanel extends StatefulWidget {
  final VoidCallback onSlidingPanelDisappear;
  final VoidCallback onSlidingPanelOpen;

  const ChallengeSlidingUpPanel({
    Key? key,
    required this.onSlidingPanelDisappear,
    required this.onSlidingPanelOpen,
  }) : super(key: key);

  @override
  State<ChallengeSlidingUpPanel> createState() =>
      _ChallengeSlidingUpPanelState();
}

class _ChallengeSlidingUpPanelState extends State<ChallengeSlidingUpPanel> {
  final MapPageController _playerPageController =
      GetIt.instance.get<MapPageController>();
  final UserService _userService = GetIt.instance.get<UserService>();

  TextEditingController controller = TextEditingController();
  PlatformFile? pickedFile;

  double _panelSlideValue = 0.0;

  final PanelController _panelController = PanelController();

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: _playerPageController,
      builder: (context, _, __) {
        final challenge = _playerPageController.challenge;
        if (challenge == null) {
          widget.onSlidingPanelDisappear();
          return Container();
        } else {
          widget.onSlidingPanelOpen();
        }

        return SlidingUpPanel(
          controller: _panelController,
          collapsed: AnimatedOpacity(
            duration: const Duration(milliseconds: 300),
            opacity: _shouldHideCollapsedPanel() ? 0 : 1,
            child: _buildCollapsedItem(challenge),
          ),
          panelBuilder: (sc) => AnimatedOpacity(
            duration: const Duration(milliseconds: 300),
            opacity: _panelSlideValue,
            child: _buildDetailsItem(challenge, sc),
          ),
          onPanelSlide: (value) {
            if (value == 0 && _panelSlideValue != 0 || value > 0.01) {
              setState(() {
                _panelSlideValue = value;
              });
            }
          },
          maxHeight: 300,
        );
      },
    );
  }

  Widget _buildCollapsedItem(Challenge challenge) {
    return Container(
      color: _getColor(challenge),
      child: Column(
        children: [
          const Center(
            child: SizedBox(
              height: 40,
              width: 50,
              child: Divider(
                color: Colors.black,
                thickness: 3,
              ),
            ),
          ),
          Text(
            challenge.title,
            style: const TextStyle(fontSize: 30),
          ),
        ],
      ),
    );
  }

  Widget _buildDetailsItem(
      Challenge challenge, ScrollController scrollController) {
    return Container(
      color: _getColor(challenge),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const Center(
            child: SizedBox(
              height: 40,
              width: 50,
              child: Divider(
                color: Colors.black,
                thickness: 3,
              ),
            ),
          ),
          Text(
            challenge.body,
            style: const TextStyle(fontSize: 20),
          ),
          if (_getState(challenge) == false) ...[
            SilidingChallengeItemBody(challenge: challenge)
          ] else
            const Center(
              child: Text(
                'Your team already done this challenge',
                style: TextStyle(fontSize: 30),
                textAlign: TextAlign.center,
              ),
            )
        ],
      ),
    );
  }

  Color _getColor(Challenge challenge) {
    Map<String, String>? request = challenge.teamState.firstWhereOrNull(
            (element) => element['teamId'] == _userService.currentUser.teamId);
    if (request == null) {
      return Colors.white;
    }
    if (request['state'] == 'denied') {
      return Colors.red.shade300;
    } else if (request['state'] == 'pending') {
      return Colors.amber.shade300;
    } else {
      return Colors.green.shade300;
    }
  }

  bool _getState(Challenge challenge) {
    Map<String, String>? request = challenge.teamState.firstWhereOrNull(
        (element) => element['teamId'] == _userService.currentUser.teamId);
    if (request == null) {
      return false;
    }
    if (request['state'] == 'accepted') {
      return true;
    } else {
      return false;
    }
  }

  bool _shouldHideCollapsedPanel() => _panelSlideValue < 0.1 ? false : true;
}

class SilidingChallengeItemBody extends StatefulWidget {
  final Challenge challenge;

  const SilidingChallengeItemBody({Key? key, required this.challenge})
      : super(key: key);

  @override
  State<SilidingChallengeItemBody> createState() => _SilidingChallengeItemBodyState();
}

class _SilidingChallengeItemBodyState extends State<SilidingChallengeItemBody> {
  final UserService _userService = GetIt.instance.get<UserService>();

  TextEditingController controller = TextEditingController();
  PlatformFile? pickedFile;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SizedBox(
          height: 50,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: TextFormField(
                controller: controller,
                decoration: const InputDecoration(
                  fillColor: Colors.yellow,
                  contentPadding: EdgeInsets.all(12),
                  hintText: 'Type your answer here...',
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                child: Image(
                  height: 100,
                  width: 100,
                  image: pickedFile != null
                      ? FileImage(File(pickedFile!.path!))
                  as ImageProvider<Object>
                      : const NetworkImage(
                    'https://firebasestorage.googleapis.com/v0/b/challenge-363613.appspot.com/o/Question_mark.png?alt=media&token=580a2ae9-f54c-4859-a2c5-3a44ded11a45',
                  ),
                ),
                onTap: () => showDialog(
                    builder: (BuildContext context) =>
                        ImageDialog(pickedFile?.path),
                    context: context),
              ),
              IconButton(
                icon: const Icon(Icons.photo),
                onPressed: () async {
                  final result = await FilePicker.platform.pickFiles();
                  if (result != null) {
                    pickedFile = result.files.first;
                    setState(() {});
                  }
                },
              ),
              IconButton(
                icon: const Icon(Icons.send),
                onPressed: () async {
                  final mapBloc = context.read<MapBloc>();
                  String? imageUrl;
                  if (pickedFile != null) {
                    imageUrl = await injector<Interactor>()
                        .uploadPicture(pickedFile!.path!, pickedFile!.name);
                  }
                  final request = Request(
                    text: controller.text,
                    date: DateTime.now(),
                    author:
                    FirebaseAuth.instance.currentUser?.email ?? 'Anonymous',
                    imageUrl: imageUrl,
                    teamId: _userService.currentUser.teamId!,
                    userId: _userService.id,
                    challengeId: widget.challenge.id!,
                    type: 'challenge',
                    //state: RequestState.pending,
                  );
                  mapBloc.add(UploadRequest(request));
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      backgroundColor: Colors.blue,
                      content: Text('Request was sent'),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
