import 'package:challenge_app/database/positioning_service.dart';
import 'package:challenge_app/view/page/challenge_sliding_up_panel.dart';
import 'package:challenge_app/view/page/mape_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../block/map/map_bloc.dart';
import '../../database/place_api_service.dart';
import '../../database/user_service.dart';
import '../../domain/model/challenge.dart';
import '../../domain/model/role.dart';
import '../dialog/create_challenge_dialog.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

const Duration _animationDuration = Duration(milliseconds: 300);

class MapPage extends StatefulWidget {
  final LatLng latLng;

  const MapPage({
    super.key,
    required this.latLng,
  });

  @override
  State<MapPage> createState() => MapPageState();
}

class MapPageState extends State<MapPage>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  final UserService _userService = GetIt.instance.get<UserService>();
  final PositioningService _positioningService =
      GetIt.instance.get<PositioningService>();
  final MapPageController _playerPageController =
      GetIt.instance.get<MapPageController>();
  final PlaceApiService _placeApiService =
      GetIt.instance.get<PlaceApiService>();

  late GoogleMapController googleMapController;

  final TextEditingController _searchController = TextEditingController();

  late CameraPosition _kGooglePlex;

  AnimationController? _positionButtonAnimationController;
  late Animation<Offset> _positionButtonOffset;
  late Animation<double> _positionButtonFade;

  bool buttonsUp = false;

  @override
  void initState() {
    super.initState();
    _positionButtonAnimationController = AnimationController(
      vsync: this,
      duration: _animationDuration,
    );
    _positionButtonOffset = Tween<Offset>(
      begin: const Offset(0, 0),
      end: const Offset(0, 2),
    ).animate(CurvedAnimation(
      parent: _positionButtonAnimationController!,
      curve: Curves.easeInOut,
    ));
    _positionButtonFade = Tween<double>(
      begin: 1,
      end: 0,
    ).animate(CurvedAnimation(
      parent: _positionButtonAnimationController!,
      curve: Curves.easeInOut,
    ));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    _kGooglePlex = CameraPosition(
      target: widget.latLng,
      zoom: 12,
    );
    return Scaffold(
      appBar: AppBar(title: const Text('Google Maps')),
      body: Stack(
        children: [
          GoogleMap(
            //liteModeEnabled: true,
            mapType: MapType.normal,
            markers: _playerPageController.markers,
            initialCameraPosition: _kGooglePlex,
            onMapCreated: (GoogleMapController controller) async {
              googleMapController = controller;
              _playerPageController.googleMapController = googleMapController;
            },
            onLongPress: (LatLng latLng) async {
              if(_userService.role == Role.admin || _userService.role == Role.manager) {
                final mapBloc = context.read<MapBloc>();
                Challenge? challenge = await showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      CreateChallengeDialog(latLng),
                );
                if (challenge != null) {
                  mapBloc.add(
                    UploadChallenge(challenge),
                  );
                }
              }
            },
            onTap: (LatLng latLng) {
              _playerPageController.challenge = null;
            },
          ),
          ChallengeSlidingUpPanel(
            onSlidingPanelDisappear: () {
              _positionButtonAnimationController!.reverse();
            },
            onSlidingPanelOpen: () {
              _positionButtonAnimationController!.forward();
            },
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              color: Colors.white,
              child: TypeAheadField(
                textFieldConfiguration: TextFieldConfiguration(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    contentPadding: const EdgeInsets.all(12),
                    hintText: 'Search for address',
                  ),
                  controller: _searchController,
                ),
                suggestionsCallback: (pattern) async {
                  final searchesBloc = context.read<MapBloc>();
                  searchesBloc.add(
                    SearchPlaces(pattern, Localizations.localeOf(context).languageCode),
                  );
                  return _playerPageController.places;
                },
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    title: Text(suggestion.description),
                  );
                },
                onSuggestionSelected: (suggestion) async {
                  _searchController.clear();
                  final latLng = await _placeApiService
                      .getPlaceDetailFromId(suggestion.placeId);

                  _playerPageController.googleMapController!.animateCamera(
                    CameraUpdate.newCameraPosition(
                      CameraPosition(
                        target: latLng,
                        zoom: 16,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Positioned(
            right: 60,
            bottom: 10,
            child: FadeTransition(
              opacity: _positionButtonFade,
              child: SlideTransition(
                position: _positionButtonOffset,
                child: ElevatedButton(
                  child: const Text('Current position'),
                  onPressed: () async {
                    LatLng latLng =
                        await _positioningService.getCurrentPosition();
                    googleMapController.animateCamera(
                      CameraUpdate.newCameraPosition(
                        CameraPosition(
                          target: latLng,
                          zoom: 16,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
