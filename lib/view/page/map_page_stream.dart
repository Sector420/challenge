import 'package:challenge_app/di/di_utils.dart';
import 'package:challenge_app/view/page/mape_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../block/map/map_bloc.dart';
import '../../domain/model/challenge.dart';
import 'map_page.dart';

class MapPageStream extends StatelessWidget {
  final MapPageController _playerPageController =
      GetIt.instance.get<MapPageController>();

  List<Challenge> challenges = [];

  LatLng latLng;

  MapPageStream({required this.latLng, super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        return injector<MapBloc>()
          ..add(LoadChallenges())
          ..add(RegisterCurrentPosition());
      },
      child: BlocBuilder<MapBloc, MapState>(
        builder: (context, state) {
          if (state is ChallengesLoaded) {
            challenges = state.challenges;
            _playerPageController.createMarkers(challenges);
          } else if (state is ChallengesEmpty) {
            _playerPageController.createMarkers([]);
          } else if (state is PositionLoaded) {
            _playerPageController.changeCurrentLocation(state.latLng);
          } else if (state is PlacesChanged) {
            _playerPageController.places = state.places;
          }
          return MapPage(
            latLng: latLng,
          );
        },
      ),
    );
  }
}
