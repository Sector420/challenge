import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../../database/user_service.dart';
import '../../domain/model/role.dart';
import '../../domain/model/search_filter.dart';

class FilterRadioGroup extends StatefulWidget {
  const FilterRadioGroup({Key? key}) : super(key: key);

  @override
  State<FilterRadioGroup> createState() => _FilterRadioGroupState();
}

class _FilterRadioGroupState extends State<FilterRadioGroup> {
  final UserService _userService = GetIt.instance.get<UserService>();
  final SearchFilter _searchFilter = GetIt.instance.get<SearchFilter>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: const Text('Challegnes'),
          leading: Radio<SearchFilterEnum>(
            value: SearchFilterEnum.challenges,
            groupValue: _searchFilter.searchFilterEnum,
            onChanged: (SearchFilterEnum? value) async {
              setState(() {
                _searchFilter.searchFilterEnum = value!;
              });
              await _searchFilter.save();
            },
          ),
        ),
        ListTile(
          title: const Text('Teams'),
          leading: Radio<SearchFilterEnum>(
            value: SearchFilterEnum.teams,
            groupValue: _searchFilter.searchFilterEnum,
            onChanged: (SearchFilterEnum? value) async {
              setState(() {
                _searchFilter.searchFilterEnum = value!;
              });
              await _searchFilter.save();
            },
          ),
        ),
        ListTile(
          title: const Text('Players'),
          leading: Radio<SearchFilterEnum>(
            value: SearchFilterEnum.players,
            groupValue: _searchFilter.searchFilterEnum,
            onChanged: (SearchFilterEnum? value) async {
              setState(() {
                _searchFilter.searchFilterEnum = value!;
              });
              await _searchFilter.save();
            },
          ),
        ),
        if(_userService.role == Role.admin || _userService.role == Role.manager)
        ListTile(
          title: const Text('Requests'),
          leading: Radio<SearchFilterEnum>(
            value: SearchFilterEnum.requests,
            groupValue: _searchFilter.searchFilterEnum,
            onChanged: (SearchFilterEnum? value) async {
              setState(() {
                _searchFilter.searchFilterEnum = value!;
              });
              await _searchFilter.save();
            },
          ),
        ),
      ],
    );
  }
}
