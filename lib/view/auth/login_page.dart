import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../../database/user_service.dart';

class LoginPage extends StatefulWidget {
  final VoidCallback onClickedSignUp;

  const LoginPage({
    Key?  key,
    required this.onClickedSignUp,
  }) : super (key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final UserService _userService = GetIt.instance.get<UserService>();

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  var _emailValid = true;
  var _passwordValid = true;

  Future<void> _tryLogin() async {
    final email = _emailController.text;
    final password = _passwordController.text;

    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
      await _userService.init();

    } on FirebaseAuthException catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(e.message!)));
    } on Exception catch (e) {
      print("Login failed: ${e.toString()}");
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text("Login failed, please try again!")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Cost Accountant"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.login_outlined,
                      size: 120,
                    ),
                  ],
                ),
                TextField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    alignLabelWithHint: true,
                    labelText: "Email address",
                    errorText: _emailValid
                        ? null
                        : "Please provide a valid email address",
                  ),
                  keyboardType: TextInputType.emailAddress,
                ),
                TextField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                    alignLabelWithHint: true,
                    labelText: "Password",
                    errorText: _passwordValid
                        ? null
                        : "The given password is invalid or not strong enough",
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () {
                            _tryLogin();
                          },
                          child: Text("Login".toUpperCase()),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom:  8.0),
                  child: GestureDetector(
                    child: const Text(
                      "Forgot password?",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.black,
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        "/forgot",
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: RichText(
                      text: TextSpan(
                        style: const TextStyle(color: Colors.black),
                        text: "No account? ",
                        children: [
                          TextSpan(
                            recognizer: TapGestureRecognizer()
                              /*..onTap = () {
                                Navigator.pushNamed(
                                  context,
                                  "/signup",
                                );
                              },*/
                              ..onTap = widget.onClickedSignUp,
                            text: "Sign up",
                            style: const TextStyle(
                              decoration: TextDecoration.underline,
                            )
                          )
                        ]
                    )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
