import 'package:challenge_app/view/auth/signup_page.dart';
import 'package:flutter/cupertino.dart';
import 'login_page.dart';

class AuthPage extends StatefulWidget {

  const AuthPage({super.key});

  @override
  _AuthPageState createState() => _AuthPageState();

}

class _AuthPageState extends State<AuthPage> {
  bool isLogin = true;

  @override
  Widget build(BuildContext context) =>
    isLogin ? LoginPage(onClickedSignUp: change,) : SignupPage(onClickedSignIn: change);

  void change() => setState(() => isLogin = !isLogin);

}