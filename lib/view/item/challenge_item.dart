import 'dart:io';

import 'package:challenge_app/block/searches/searches_bloc.dart';
import 'package:challenge_app/view/dialog/image_dialog.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../database/user_service.dart';
import '../../di/di_utils.dart';
import '../../domain/interactor/interactor.dart';
import '../../domain/model/challenge.dart';
import '../../domain/model/request.dart';
import '../../domain/model/role.dart';
import '../page/mape_page_controller.dart';
import 'package:collection/collection.dart';

class ChallengeItem extends StatelessWidget {
  final MapPageController _mapPageController =
      GetIt.instance.get<MapPageController>();
  final UserService _userService = GetIt.instance.get<UserService>();

  final Challenge challenge;

  ChallengeItem(this.challenge, {super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          Expanded(
            child: Card(
              color: _getColor(),
              child: Row(
                children: [
                  IconButton(
                    iconSize: 40,
                    color: Colors.red,
                    icon: const Icon(Icons.location_on),
                    onPressed: () {
                      _mapPageController.tabIndex = 1;
                      _mapPageController.currentLatLng = LatLng(
                        challenge.latLng.latitude,
                        challenge.latLng.longitude,
                      );
                    },
                  ),
                  Expanded(
                    child: ExpansionTile(
                      title: Text(challenge.title),
                      subtitle: Text(challenge.body),
                      children: [
                        if(_userService.role != Role.player)
                          Container()
                        else if (_getState() == false)
                          ChallengeItemBody(challenge: challenge)
                        else
                          const Center(
                            child: Text(
                              'Your team already done this challenge',
                              style: TextStyle(fontSize: 30),
                              textAlign: TextAlign.center,
                            ),
                          )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Color _getColor() {
    Map<String, String>? request = challenge.teamState.firstWhereOrNull(
        (element) => element['teamId'] == _userService.currentUser.teamId);
    if (request == null) {
      return Colors.white;
    }
    if (request['state'] == 'denied') {
      return Colors.red.shade300;
    } else if (request['state'] == 'pending') {
      return Colors.amber.shade300;
    } else {
      return Colors.green.shade300;
    }
  }

  bool _getState() {
    Map<String, String>? request = challenge.teamState.firstWhereOrNull(
        (element) => element['teamId'] == _userService.currentUser.teamId);
    if (request == null) {
      return false;
    }
    if (request['state'] == 'accepted') {
      return true;
    } else {
      return false;
    }
  }
}

class ChallengeItemBody extends StatefulWidget {
  final Challenge challenge;

  const ChallengeItemBody({Key? key, required this.challenge})
      : super(key: key);

  @override
  State<ChallengeItemBody> createState() => _ChallengeItemBodyState();
}

class _ChallengeItemBodyState extends State<ChallengeItemBody> {
  final UserService _userService = GetIt.instance.get<UserService>();

  TextEditingController controller = TextEditingController();
  PlatformFile? pickedFile;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SizedBox(
          height: 50,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: TextFormField(
                controller: controller,
                decoration: const InputDecoration(
                  fillColor: Colors.yellow,
                  contentPadding: EdgeInsets.all(12),
                  hintText: 'Type your answer here...',
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                child: Image(
                  height: 100,
                  width: 100,
                  image: pickedFile != null
                      ? FileImage(File(pickedFile!.path!))
                          as ImageProvider<Object>
                      : const NetworkImage(
                          'https://firebasestorage.googleapis.com/v0/b/challenge-363613.appspot.com/o/Question_mark.png?alt=media&token=580a2ae9-f54c-4859-a2c5-3a44ded11a45',
                        ),
                ),
                onTap: () => showDialog(
                    builder: (BuildContext context) =>
                        ImageDialog(pickedFile?.path),
                    context: context),
              ),
              IconButton(
                icon: const Icon(Icons.photo),
                onPressed: () async {
                  final result = await FilePicker.platform.pickFiles();
                  if (result != null) {
                    pickedFile = result.files.first;
                    setState(() {});
                  }
                },
              ),
              IconButton(
                icon: const Icon(Icons.send),
                onPressed: () async {
                  final searchBloc = context.read<SearchesBloc>();
                  String? imageUrl;
                  if (pickedFile != null) {
                    imageUrl = await injector<Interactor>()
                        .uploadPicture(pickedFile!.path!, pickedFile!.name);
                  }
                  final request = Request(
                    text: controller.text,
                    date: DateTime.now(),
                    author:
                        FirebaseAuth.instance.currentUser?.email ?? 'Anonymous',
                    imageUrl: imageUrl,
                    teamId: _userService.currentUser.teamId!,
                    userId: _userService.id,
                    challengeId: widget.challenge.id!,
                    type: 'challenge',
                    //state: RequestState.pending,
                  );
                  searchBloc.add(UploadRequest(request));
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      backgroundColor: Colors.blue,
                      content: Text('Request was sent'),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
