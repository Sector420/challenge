import 'package:challenge_app/domain/model/challenge_user.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import '../../block/searches/searches_bloc.dart';
import '../../database/user_service.dart';
import '../../domain/model/challenge.dart';
import '../../domain/model/request.dart';
import '../../domain/model/team.dart';
import 'package:collection/collection.dart';

class RequestItem extends StatelessWidget {
  final UserService _userService = GetIt.instance.get<UserService>();

  final Request request;

  RequestItem(this.request, {super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          Expanded(
            child: FutureBuilder<List<dynamic>>(
              future: Future.wait([
                _getChallenge(),
                _getTeam(),
                _getUser(),
                _getState(),
              ]),
              builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
                if (!snapshot.hasData) {
                  return const LinearProgressIndicator();
                }
                if(snapshot.data![0] == null) {
                  return const Text('nonono');
                }
                List<dynamic> searches = snapshot.data!;
                Challenge challenge = searches[0] as Challenge;
                Team team = searches[1] as Team;
                ChallengeUser user = searches[2] as ChallengeUser;
                bool state = searches[3] as bool;
                if(state == true) {
                  return Card(
                    color: Colors.white,
                    child: Row(
                      children: [
                        IconButton(
                          iconSize: 40,
                          color: Colors.red,
                          icon: const Icon(Icons.location_on),
                          onPressed: () {},
                        ),
                        Expanded(
                          child: ExpansionTile(
                            title: Text(challenge.title),
                            subtitle: Text('${team.title} ${user.username}'),
                            children: [
                              RequestItemBody(request: request),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }
                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<Challenge?> _getChallenge() async {
    final snapshot = await FirebaseFirestore.instance
        .collection('challenges')
        .doc(request.challengeId)
        .get();
    if(snapshot.exists) {
      return Challenge.fromJson(snapshot.data()!, snapshot.id);
    }
    return null;
  }

  Future<Team?> _getTeam() async {
    final snapshot = await FirebaseFirestore.instance
        .collection('teams')
        .doc(request.teamId)
        .get();
    if(snapshot.exists) {
      return Team.fromJson(snapshot.data()!);
    }
    return null;
  }

  Future<ChallengeUser?> _getUser() async {
    final snapshot = await FirebaseFirestore.instance
        .collection('users')
        .doc(request.userId)
        .get();
    if(snapshot.exists) {
      return ChallengeUser.fromJson(snapshot.data()!, snapshot.id);
    }
    return null;
  }

  Future<bool> _getState() async {
    final snapshot = await FirebaseFirestore.instance
        .collection('challenges')
        .doc(request.challengeId)
        .get();

    Challenge? challenge;
    if(snapshot.exists) {
      challenge = Challenge.fromJson(snapshot.data()!, snapshot.id);
      Map<String, String>? request1 = challenge.teamState.firstWhereOrNull(
              (element) => element['teamId'] == _userService.currentUser.teamId);
      if (request1 == null) {
        return false;
      }
      if (request1['state'] == 'pending') {
        return true;
      } else {
        return false;
      }
    }
    else {
      return false;
    }
  }
}

class RequestItemBody extends StatefulWidget {
  final Request request;

  const RequestItemBody({Key? key, required this.request}) : super(key: key);

  @override
  State<RequestItemBody> createState() => _RequestItemBodyState();
}

class _RequestItemBodyState extends State<RequestItemBody> {
  TextEditingController controller = TextEditingController();
  PlatformFile? pickedFile;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 50,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Text(widget.request.text),
            ),
          ),
        ),
        if (widget.request.imageUrl != null)
          InkWell(
            child: Image(
                height: 100,
                width: 100,
                image: NetworkImage(widget.request.imageUrl!)),
            onTap: () => showDialog(
              builder: (BuildContext context) => Dialog(
                backgroundColor: Colors.white,
                insetPadding: const EdgeInsets.all(2),
                child: Container(
                  height: 500,
                  width: 200,
                  decoration: const BoxDecoration(),
                  child: Expanded(
                    child: Image(
                      image: NetworkImage(
                        widget.request.imageUrl!,
                      ),
                    ),
                  ),
                ),
              ),
              context: context,
            ),
          ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                icon: const Icon(Icons.clear),
                onPressed: () async {
                  final searchBloc = context.read<SearchesBloc>();
                  searchBloc.add(UpdateRequestState(widget.request.id!, widget.request.challengeId, 'denied'));
                },
              ),
              IconButton(
                icon: const Icon(Icons.check),
                onPressed: () async {
                  final searchBloc = context.read<SearchesBloc>();
                  searchBloc.add(UpdateRequestState(widget.request.id!, widget.request.challengeId, 'accepted'));
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
