import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../domain/model/message.dart';

class MessageItem extends StatelessWidget {
  final user = FirebaseAuth.instance.currentUser;
  final Message message;

  MessageItem(this.message, {super.key});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: message.author == user!.email!
          ? Alignment.centerRight
          : Alignment.centerLeft,
      child: Card(
        color: Colors.lightBlue,
        elevation: 8,
        child: Column(
          children: [
            if (message.imageUrl == null)
              Padding(
                padding: const EdgeInsets.all(12),
                child: Text(message.text),
              ),
            if (message.imageUrl != null)
              Image.network(
                message.imageUrl!,
                height: 200,
                fit: BoxFit.fill,
              )
          ],
        ),
      ),
    );
  }
}
