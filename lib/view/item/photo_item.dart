import 'package:flutter/material.dart';

import '../page/gallery_page.dart';

class PhotoItem extends StatelessWidget {
  final int index;
  final List<String> urlList;

  const PhotoItem(this.index, this.urlList, {super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Image.network(
        urlList[index],
        fit: BoxFit.cover,
      ),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => GalleryPage(urlList: urlList, index: index),
          ),
        );
      },
    );
  }
}
