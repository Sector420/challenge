import 'package:challenge_app/block/searches/searches_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../domain/model/challenge_user.dart';
import '../../domain/model/role.dart';
import '../dialog/change_role_dialog.dart';


class ChallengeUserItem extends StatelessWidget {
  final ChallengeUser challengeUser;

  ChallengeUserItem(this.challengeUser, {super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          Expanded(
            child: InkWell(
              onTap: () async {
                final searchBloc = context.read<SearchesBloc>();
                final result = await showDialog(
                    context: context,
                    builder: (context) => ChangeRoleDialog(user: challengeUser));
                if (result != null) {
                  searchBloc.add(UpdateUserRole(challengeUser.id!, result));
                }
              },
              child: Card(
                color: Colors.lightBlueAccent,
                child: Row(
                  children: [
                     Padding(
                      padding: const EdgeInsets.all(15),
                      child: CircleAvatar(
                        radius: 30,
                        backgroundImage: NetworkImage(challengeUser.imageUrl),
                      ),
                    ),
                    Expanded(
                      child: ListTile(
                        title: Text(challengeUser.username),
                        subtitle: Text(challengeUser.email),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 30),
                      child: Text(
                        challengeUser.point.toString(),
                        style: const TextStyle(fontSize: 30),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
