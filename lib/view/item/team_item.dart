import 'package:flutter/material.dart';

import '../../domain/model/team.dart';

class TeamItem extends StatelessWidget {
  final Team team;

  const TeamItem(this.team, {super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          Expanded(
            child: Card(
              color: Colors.lightBlueAccent,
              child: Row(
                children: [
                  Expanded(
                    child: ListTile(
                      title: Text(team.title),
                      subtitle: Text(team.author),
                    ),
                  ),
                  Text(
                    team.point.toString(),
                    style: const TextStyle(fontSize: 30),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
