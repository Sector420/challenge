import 'package:challenge_app/domain/model/challenge.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class JoinTeamDialog extends StatefulWidget {

  const JoinTeamDialog({super.key});

  @override
  _JoinTeamDialogState createState() => _JoinTeamDialogState();
}

class _JoinTeamDialogState extends State<JoinTeamDialog> {
  final _teamIdController = TextEditingController();
  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _teamIdController,
              decoration: InputDecoration(
                alignLabelWithHint: true,
                labelText: 'Team id',
                errorText: _validate ? 'Value Can\'t Be Empty' : null,
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                setState(() {
                  _teamIdController.text.isEmpty
                      ? _validate = true
                      : _validate = false;
                });
                if (!_validate) {
                  Navigator.pop(context, _teamIdController.text);
                }
              },
              child: Text('Post'.toUpperCase()),
            ),
          ],
        ),
      ),
    );
  }
}
