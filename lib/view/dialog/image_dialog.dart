import 'dart:io';

import 'package:flutter/material.dart';

class ImageDialog extends StatelessWidget {
  String? path;

  ImageDialog(this.path, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      insetPadding: const EdgeInsets.all(2),
      child: Container(
        height: 500,
        width: 200,
        decoration: const BoxDecoration(),
        child: Expanded(
          child: Image(
            image: path != null
                ? FileImage(File(path!))
            as ImageProvider<Object>
                : const NetworkImage(
              'https://firebasestorage.googleapis.com/v0/b/challenge-363613.appspot.com/o/Question_mark.png?alt=media&token=580a2ae9-f54c-4859-a2c5-3a44ded11a45',
            ),
          ),
        ),
      ),
    );
  }
}
