import 'package:challenge_app/domain/model/challenge_user.dart';
import 'package:flutter/material.dart';

import '../../domain/model/role.dart';

class ChangeRoleDialog extends StatefulWidget {
  final ChallengeUser user;

  const ChangeRoleDialog({
    super.key,
    required this.user,
  });

  @override
  State<ChangeRoleDialog> createState() => _ChangeRoleDialogState();
}

class _ChangeRoleDialogState extends State<ChangeRoleDialog> {
  final _formKey = GlobalKey<FormState>();
  late final Role role; // note used for communication

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: AlertDialog(
        title: const Text("Szerep kiválasztás"),
        content: OptionsInputWidget<Role>(
          initialValue: Role.fromString(widget.user.role),
          values: Role.values,
          onSaved: (value) => role = value,
          toDisplay: (value) => value.hungarianDisplayName,
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('Mégse'),
          ),
          TextButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState!.save();
                Navigator.pop(context, role.name);
              }
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }
}

class OptionsInputWidget<T extends Object> extends StatefulWidget {
  final List<T> values;
  final T initialValue;
  final void Function(T) onSaved;
  final String Function(T) toDisplay;
  final FocusNode? focusNode;

  const OptionsInputWidget({
    required this.values,
    required this.initialValue,
    required this.onSaved,
    required this.toDisplay,
    this.focusNode,
    super.key,
  });

  @override
  State<OptionsInputWidget<T>> createState() => _OptionsInputWidgetState<T>();
}

class _OptionsInputWidgetState<T extends Object>
    extends State<OptionsInputWidget<T>> {
  late T value;

  @override
  void initState() {
    super.initState();
    value = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<T>(
      focusNode: widget.focusNode,
      value: value,
      items: [
        for (final t in widget.values)
          DropdownMenuItem(
            value: t,
            child: Text(widget.toDisplay(t)),
          ),
      ],
      onChanged: (value) {
        if (value != null) {
          setState(() => this.value = value);
        }
      },
      onSaved: (value) => widget.onSaved(this.value),
    );
  }
}
