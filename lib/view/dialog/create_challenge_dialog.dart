import 'package:challenge_app/domain/model/challenge.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CreateChallengeDialog extends StatefulWidget {
  final LatLng latLng;

  const CreateChallengeDialog(this.latLng, {super.key});

  @override
  _CreateChallengeDialogState createState() => _CreateChallengeDialogState();
}

class _CreateChallengeDialogState extends State<CreateChallengeDialog> {
  final _eventTitleController = TextEditingController();
  final _eventBodyController = TextEditingController();
  final user = FirebaseAuth.instance.currentUser;
  bool _validate = false;
  Level level = Level.easy;

  Challenge _createEvent(BuildContext context) {
    Challenge newChallenge = Challenge(
      author: FirebaseAuth.instance.currentUser?.email ?? 'Anonymous',
      title: _eventTitleController.text,
      date: DateTime.now(),
      body: _eventBodyController.text,
      latLng: GeoPoint(widget.latLng.latitude, widget.latLng.longitude),
      level: level,
      teamState: [],
    );
    return newChallenge;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _eventTitleController,
              decoration: InputDecoration(
                alignLabelWithHint: true,
                labelText: 'Title',
                errorText: _validate ? 'Value Can\'t Be Empty' : null,
              ),
            ),
            TextField(
              controller: _eventBodyController,
              decoration: const InputDecoration(
                alignLabelWithHint: true,
                labelText: 'Write information about your event',
              ),
            ),
            ListTile(
              title: const Text('Easy'),
              leading: Radio<Level>(
                value: Level.easy,
                groupValue: level,
                onChanged: (Level? value) async {
                  setState(() {
                    level = value!;
                  });
                },
              ),
            ),
            ListTile(
              title: const Text('Normal'),
              leading: Radio<Level>(
                value: Level.normal,
                groupValue: level,
                onChanged: (Level? value) async {
                  setState(() {
                    level = value!;
                  });
                },
              ),
            ),
            ListTile(
              title: const Text('Hard'),
              leading: Radio<Level>(
                value: Level.hard,
                groupValue: level,
                onChanged: (Level? value) async {
                  setState(() {
                    level = value!;
                  });
                },
              ),
            ),
            //LevelRadioGroup(level),
            ElevatedButton(
              onPressed: () async {
                setState(() {
                  _eventTitleController.text.isEmpty
                      ? _validate = true
                      : _validate = false;
                });
                if (!_validate) {
                  final newEvent = _createEvent(context);
                  Navigator.pop(context, newEvent);
                }
              },
              child: Text('Post'.toUpperCase()),
            ),
          ],
        ),
      ),
    );
  }
}
