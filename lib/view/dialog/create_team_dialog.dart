import 'package:challenge_app/domain/model/team.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class CreateTeamDialog extends StatefulWidget {
  const CreateTeamDialog({super.key});

  @override
  _CreateTeamDialogState createState() => _CreateTeamDialogState();
}

class _CreateTeamDialogState extends State<CreateTeamDialog> {
  final _eventTitleController = TextEditingController();
  final user = FirebaseAuth.instance.currentUser;
  bool _validate = false;

  Team _createTeam(BuildContext context) {
    List<String> users = [];
    users.add(user!.email!);
    final newTeam = Team(
      author: FirebaseAuth.instance.currentUser?.email ?? "Anonymous",
      title: _eventTitleController.text,
      point: 0,
    );
    return newTeam;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _eventTitleController,
              decoration: InputDecoration(
                alignLabelWithHint: true,
                labelText: "Title",
                errorText: _validate ? 'Value Can\'t Be Empty' : null,
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            ElevatedButton(
              onPressed: () async {
                setState(() {
                  _eventTitleController.text.isEmpty
                      ? _validate = true
                      : _validate = false;
                });
                if (!_validate) {
                  final newTeam = _createTeam(context);
                  Navigator.pop(context, newTeam);
                }
              },
              child: Text('Send request'.toUpperCase()),
            ),
          ],
        ),
      ),
    );
  }
}
