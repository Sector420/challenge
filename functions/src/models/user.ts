export type User = {
    username: string;
    email: string;
    imageUrl: string;
    role: string;
    teamId?: string;
    point: number;
};
