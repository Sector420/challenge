import * as admin from "firebase-admin";

export enum Level {
  easy = "easy",
  normal = "normal",
  hard = "hard",
}

export type Challenge = {
  id?: string;
  author: string;
  title: string;
  date: admin.firestore.Timestamp;
  body: string;
  latLng: admin.firestore.GeoPoint;
  level: Level;
  teamState: {
    teamId: string;
    requestId: string;
    state: string;
  }[];
};
