import * as admin from "firebase-admin";

export type Request = {
  text: string;
  date: admin.firestore.Timestamp;
  author: string;
  imageUrl?: string;
  teamId: string;
  userId: string;
  challengeId: string;
  type: string;
};
