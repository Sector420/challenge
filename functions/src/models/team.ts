export type Team = {
    author: string;
    title: string;
    point: number;
  };