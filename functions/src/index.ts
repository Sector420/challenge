import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { User } from "./models/user";
import { Challenge } from "./models/challenge";
import { Request } from "./models/request";
import { Team } from "./models/team";

type Change<T> = functions.Change<T>;
type DocumentData = admin.firestore.DocumentData;
type DocumentSnapshot<T = DocumentData> = admin.firestore.DocumentSnapshot<T>;
type DocumentReference<T = DocumentData> = admin.firestore.DocumentReference<T>;

admin.initializeApp();

const db = admin.firestore();

const region = "europe-central2";

const challenges = "challenges";
const requests = "requests";
const teams = "teams";
const users = "users";

export const onRequestCreate = functions
  .region(region)
  .firestore.document(`${requests}/{requestId}`)
  .onCreate(async (snapshot, context) => {
    const request = snapshot.data()! as Request;
    const requestId = context.params.requestId;

    const challengeReference = db
      .collection(challenges)
      .doc(request.challengeId) as DocumentReference<Challenge>;

    const challengeSnapshot = await challengeReference.get();

    const challengeData = challengeSnapshot.data()!;

    if (
      challengeData.teamState.find((t) => t.requestId === requestId) === undefined
    ) {
      challengeData.teamState.push({teamId: request.teamId, requestId: requestId, state: "pending" });
      await challengeReference.update(challengeData);
    }
  });

export const onRequestUpdate = functions
  .region(region)
  .firestore.document(`${requests}/{requestId}`)
  .onUpdate(async (_change, context) => {
    const change = _change as Change<DocumentSnapshot<Request>>;
    const requestBefore = change.before.data()!;
    const requestId = context.params.requestId;

    const challengeReference = db
      .collection(challenges)
      .doc(requestBefore.challengeId) as DocumentReference<Challenge>;

    const challengeSnapshot = await challengeReference.get();

    const challengeData = challengeSnapshot.data()!;

    const requestState = challengeData.teamState.find((t) => t.requestId === requestId);
    if (
      requestState!.state === "denied"
    ) {
      const index = challengeData.teamState.indexOf(requestState!);
      challengeData.teamState.splice(index, 1);
      challengeData.teamState.push({teamId: requestBefore.teamId, requestId: requestId, state: "pending" });
      await challengeReference.update(challengeData);
    }
  });

export const onChallengeUpdate = functions
  .region(region)
  .firestore.document(`${challenges}/{challengeId}`)
  .onUpdate(async (_change, context) => {
    const change = _change as Change<DocumentSnapshot<Challenge>>;
    const teamStateBeforeArray = change.before.data()!.teamState;
    const teamStateAfterArray = change.after.data()!.teamState;
    let before: {
      requestId: string;
      state: string;
    };
    let after: {
      requestId: string;
      state: string;
    };
    
    for (let i = 0; i < teamStateBeforeArray.length; i++) {
      if(teamStateBeforeArray[i].state !== teamStateAfterArray[i].state) {
          before = teamStateBeforeArray[i];
          after = teamStateAfterArray[i];
          break;
      }
    }
    
    if(before!.state === "pending" && after!.state === "accepted") {
      let point: number;
      const level: string = change.before.data()!.level;
      if(level === "easy") {
        point = 1;
      } else if (level === "normal") {
        point = 2;
      } else {
        point = 3;
      }

      const requestReference = db
      .collection(requests)
      .doc(before!.requestId) as DocumentReference<Request>;

      const requestSnapshot = await requestReference.get();

      const requestData = requestSnapshot.data()!;

      const teamReference = db
      .collection(teams)
      .doc(requestData.teamId) as DocumentReference<Team>;

      const teamSnapshot = await teamReference.get();

      const teamData = teamSnapshot.data()!;

      teamData.point += point;

      await teamReference.update(teamData);

      const userReference = db
      .collection(users)
      .doc(requestData.userId) as DocumentReference<User>;

      const userSnapshot = await userReference.get();

      const userData = userSnapshot.data()!;

      userData.point += point;

      await userReference.update(userData);
    }
  });

